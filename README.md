# CONFIG

## INSTALL DEPENDENCIES
```shell
npm install
```

- Create a new **.env.local** file at the project root, and insert this variable: with **your moviedb API key** 

```env
REACT_APP_KEY_API=<APIKEY>
```

## LAUNCH PROJECT
```shell
npm run start
```