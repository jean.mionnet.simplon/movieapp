import './List.scss';
import React, { useState, useEffect } from 'react';
import {
    Link,
    useParams
} from "react-router-dom";

function List() {
    let params = useParams();
    const [error, setIsError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(0);
    const [totalResults, settotalResults] = useState(null);
    
    useEffect(() => {
        fetch(`https://api.themoviedb.org/3/movie/${params.filter}?api_key=${process.env.REACT_APP_KEY_API}&page=${currentPage}&language=en-US`)
          .then(res => res.json())
          .then(
            (result) => {
                setItems(result.results);
                setTotalPages(result.total_pages);
                settotalResults(result.total_results);
                setIsLoaded(true);
            },
            (error) => {
              setIsError(true);
              setIsLoaded(true);
            }
          )
    });

    if(error) {
        return <div className="List mt-5">Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return(
            <div className="List mt-5">
                <h3>Loading...</h3>
            </div>
        ) 
    } else {
        return (
            <div className="List mt-5 p-4">
                <ul className="row g-0">
                    {items.map(item => (
                        <Link to={"/detail/" + item.id} key={item.id} className="card col-md-3">
                            <div>
                                <div className="card-header">
                                    <img src={'https://image.tmdb.org/t/p/w500/' + item.poster_path} alt={'Poster of ' + item.title}></img>
                                </div>
                                <div className="card-body">
                                    <h3>{item.name ? item.name : item.title}</h3>
                                    <p class="m-0">{item.release_date}</p>
                                    <p class="m-0">{item.vote_average}/10</p>
                                </div>
                            </div>
                        </Link>
                    ))}
                </ul>
                <div className="pagination d-flex flex-column justify-content-center align-items-center">
                    <div className="text-center">
                        <p>{items.length > 1 ? items.length + ' results on this page' : items.length + ' result on this page'}</p>
                        <p>{totalResults > 1 ? totalResults + ' results' : totalResults + ' result'}</p>
                        <p>Current page: <strong>{currentPage}</strong></p>
                    </div>
                    <div className="btn-group mt-4">
                        <button onClick={() => setCurrentPage(1)} disabled={currentPage === 1}>&lt;&lt;</button>
                        <button onClick={() => setCurrentPage(currentPage - 1)} disabled={currentPage === 1} >&lt;</button>
                        <button onClick={() => setCurrentPage(currentPage + 1)} disabled={currentPage === totalPages}>&gt;</button>
                        <button onClick={() => setCurrentPage(totalPages)} disabled={currentPage === totalPages} >&gt;&gt;</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default List;
