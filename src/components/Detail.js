import './Detail.scss';
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function Detail() {
    let params = useParams();
    const [error, setIsError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [item, setItem] = useState([]);

    useEffect(() => {
        fetch(`https://api.themoviedb.org/3/movie/${params.id}?api_key=${process.env.REACT_APP_KEY_API}&language=en-US`)
          .then(res => res.json())
          .then(
            (result) => {
                setItem(result);
                setIsLoaded(true);
            },
            (error) => {
              setIsError(true);
              setIsLoaded(true);
            }
          )
    }, []);

    if(error) {
        return <div className="Detail mt-5">Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return(
            <div className="Detail mt-5">
                <h3>Loading...</h3>
            </div>
        ) 
    } else {
        return (
            <div className="Detail">
                <div className="poster-wrap">
                    <img src={'https://image.tmdb.org/t/p/original/' + item.backdrop_path} alt={'Poster of ' + item.title} />
                    <div className="catchline">
                        <h1 class="mb-3">{item.name ? item.name : item.title}</h1>
                        <h2>{item.name ? item.name : item.tagline}</h2>
                    </div>
                </div>
                <div className="p-5">
                    <div className="row">
                        <div className="col-md-6">
                            <ul className="d-flex flex-row">
                                {item.genres.map(genre => (
                                    <li className="label pe-3">#{genre.name}</li>
                                ))}
                            </ul>
                            <h3 className="label">Overview</h3>
                            <p className="light">{item.overview}</p>
                            <h5><span className="label">Average: </span>{item.vote_average}/10 ({item.vote_count} votes)</h5>
                        </div>
                        <div className="col-md-6">
                            <h5><span className="label">Status: </span>{item.status}</h5>
                            <h5><span className="label">Release date: </span>{item.release_date}</h5>
                            <h5><span className="label">Budget: </span>{item.budget}$</h5>
                            <h5><span className="label">Original language: </span><span className="text-uppercase">{item.original_language}</span></h5>
                            {item.homepage && <h5><a href={item.homepage} className="label" target="_blank" rel="noreferrer">Official website ➡</a></h5>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Detail;
