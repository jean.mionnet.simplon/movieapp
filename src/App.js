import './App.scss';
import React from "react";
import List from './components/List';
import Detail from './components/Detail';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="p-4">
          <ul>
            <li>
              <NavLink to="/list/top_rated" activeClassName='is-active'>Top Rated</NavLink>
            </li>
            <li class="ps-3">
              <NavLink to="/list/popular" activeClassName='is-active'>Popular</NavLink>
            </li>
          </ul>
        </nav>

        <Switch>
          <Route path="/list/:filter" children={<List />} />
          <Route path="/detail/:id" children={<Detail />} />
        </Switch>

      </div>
    </Router>

  );
}

export default App;
